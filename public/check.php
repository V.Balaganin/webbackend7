<?php
session_start();
include('csrf.class.php');
$csrf = new csrf();
$token_id = $csrf->get-token_id();
$token_value = $csrf->get_token($token_id);

$form_names = $csrf->get_token($token_id);

if (isset($_POST['login'])) {
    $login = $_POST['login'];
    if ($login == '') {
        unset($login);
    }
}
if (isset($_POST['pass'])) {
    $pass = $_POST['pass'];
    if ($pass == '') {
        unset($pass);
    }
}

if (empty($login) || empty($pass)) {
    exit("Вы ввели не всю информацию, вернитесь назад и заполните все поля!");
}

$login = htmlspecialchars(trim($login));
$pass = htmlspecialchars(md5(trim($pass)));

include("storage.php");

$query = $db->prepare("SELECT * FROM `users` WHERE `login` = ?");
$query->execute([$login]);
$row = $query->fetch();
if (empty($row['login'])) {
    exit("Извините, введённый вами login или пароль неверный.");
}
else {
    if ($row['pass'] == $pass) {
        $_SESSION['login'] = $row['login']; 
        $_SESSION['id'] = $row['id'];
        echo "Вы успешно вошли на сайт! <a href='index.php'>Главная страница</a>";
    }
    else {
        exit("Извините, login или пароль неверный.");
    }
}

?>